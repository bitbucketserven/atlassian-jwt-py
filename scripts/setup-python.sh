#! /bin/bash

# setup Python 2.7
apt-get install -y --no-install-recommends python2.7
curl https://bootstrap.pypa.io/get-pip.py -sSf | python2.7

# setup Python 3.5
pip3.5 install -U pip

for pip in pip2.7 pip3.5; do
    ${pip} -V
    ${pip} install pytest
    ${pip} install -r requirements.txt
    ${pip} install .
done
