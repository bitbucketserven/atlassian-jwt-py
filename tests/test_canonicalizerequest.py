import atlassian_jwt_compliance
import pytest

from atlassian_jwt.url_utils import canonicalize_request

_post_tests = [
    ('POST',
     'quux/blick?foo=%7Bsomething%7D&bar={other.thing}&True',
     'POST&/quux/blick&True=&bar=%7Bother.thing%7D&foo=%7Bsomething%7D'),

    ('POST',
     '',
     'POST&/&'),

    ('POST',
     '?key=value2&key=value1',
     'POST&/&key=value1,value2'),

    ('POST',
     '?key= +*~',
     'POST&/&key=%20%20%2A~'),

    ('POST',
     '?key=*&key=%2B',
     'POST&/&key=%2A,%2B'),

    ('POST',
     'https://bar.net/sizzle',
     'POST&/sizzle&'),
]


def parameters():
    """\
    Generate the parameter for the test. Returns tuples of (method, url,
    expected_url).
    """
    for test in _post_tests:
        yield test

    # Test are the parameters from the JwtSigningInteroperabilityTest
    for test in atlassian_jwt_compliance.tests:
        yield ('GET', test['relativeUrl'], test['canonicalUrl'])


@pytest.mark.parametrize("http_method, url, expected", parameters())
def test_canonicalize_request(http_method, url, expected):
    assert canonicalize_request(http_method, url) == expected


