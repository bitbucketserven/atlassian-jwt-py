import json
import os
import re

from uuid import UUID


def _load_data():
    """\
    Reads in data from atlassian_jwt_compliance.json. This file is generated
    using the JwtSigningInteroperabilityTest from
    https://stash.atlassian.com/projects/ac/repos/atlassian-connect.
    """
    jwt_re = re.compile(r'[&?]jwt=(?P<jwt>[^&]+)$')
    prefix = 'https://example.com'

    def remove_prefix(start, string):
        if string.startswith(start):
            return string[len(start):]
        else:
            return string

    def process_test(test_data):
        result = {}
        signed_url = test_data['signedUrl']
        jwt_match = jwt_re.search(signed_url)
        if not jwt_match:
            raise ValueError('URL does not have a JWT token at the end: %s'
                             % signed_url)
        result['jwt'] = jwt_match.group('jwt')
        result['relativeSignedUrl'] = remove_prefix(prefix, signed_url)

        url = signed_url[:jwt_match.start()]
        result['url'] = url
        result['relativeUrl'] = remove_prefix(prefix, url)
        result['canonicalUrl'] = test_data['canonicalUrl']

        return result

    this_dir, _ = os.path.split(__file__)
    data_path = os.path.join(this_dir, 'atlassian_jwt_compliance.json')

    with open(data_path) as f:
        data = json.load(f)

    parsed_secret = UUID(data['secret'])
    parsed_tests = map(process_test, data['tests'])
    return parsed_secret, parsed_tests


secret, tests = _load_data()
